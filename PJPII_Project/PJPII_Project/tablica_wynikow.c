#include <stdlib.h>
#include <stdio.h>
#include "data.h"
#include "Objekty.h"

extern opcje_gry dane_gry;
gracz wyniki[5] = { 0 };

void tablica_wynikow(ALLEGRO_DISPLAY *display)
{
	FILE *tablica_wynikow = NULL;


	errno_t err = fopen_s(&tablica_wynikow, "tablica_wynikow.ini", "r");

	if (err)
	{
		FILE *tablica_wynikow_create = NULL;
		err = fopen_s(&tablica_wynikow_create, "tablica_wynikow.ini", "w");

		if (err)
		{
			al_show_native_message_box(display, "Error", "Error", "Nie udalo sie stworzyc pliku tablica_wynikow.ini", 0, ALLEGRO_MESSAGEBOX_ERROR);
			exit(EXIT_FAILURE);
		}

		for (size_t i = 0; i < 5; i++)
		{
			fprintf(tablica_wynikow_create, "%i.NICK 0\n", i + 1);
		}

		fclose(tablica_wynikow_create);
		return;
	}

	char tablica_wynikow_str[64] = { 0 };

	for (size_t i = 0; i < 5; i++)
	{
		fgets(tablica_wynikow_str, sizeof(tablica_wynikow_str), tablica_wynikow);

		sscanf_s(tablica_wynikow_str, "%i.%s %i", &wyniki[i].ID, wyniki[i].nick, sizeof(wyniki[i].nick), &wyniki[i].wynik);
	}

	fclose(tablica_wynikow);

}

void sortuj_wyniki(ALLEGRO_DISPLAY *display, char* nick, int wynik)
{
	FILE *tablica_wynikow = NULL;

	errno_t err = fopen_s(&tablica_wynikow, "tablica_wynikow.ini", "r");

	if (err)
	{
		al_show_native_message_box(display, "Error", "Error", "Nie udalo sie otworzyc pliku tablica_wynikow.ini", 0, ALLEGRO_MESSAGEBOX_ERROR);
		exit(EXIT_FAILURE);
	}


	char tablica_wynikow_str[64] = { 0 };
	int tymczasowe_wyniki[6][2] = { 0 };
	for (size_t i = 0; i < 5; i++)
	{
		fgets(tablica_wynikow_str, sizeof(tablica_wynikow_str), tablica_wynikow);
		printf("%s\n", tablica_wynikow_str);

		sscanf_s(tablica_wynikow_str, "%i.%*s %i", &tymczasowe_wyniki[0][i], &tymczasowe_wyniki[1][i]);
	}

	fclose(tablica_wynikow);

	err = fopen_s(&tablica_wynikow, "tablica_wynikow.ini", "w");

	tymczasowe_wyniki[5][0] = -1;
	tymczasowe_wyniki[5][1] = wynik;

	for (size_t c = 0; c < (6 - 1); c++)
	{
		for (size_t d = 0; d < 6 - c - 1; d++)
		{
			if (tymczasowe_wyniki[1][d] < tymczasowe_wyniki[1][d])
			{
				int swap = tymczasowe_wyniki[1][d];
				int id = tymczasowe_wyniki[0][d];
				char nick_temp[8] = { 0 };
				strncpy_s(nick_temp, 8, wyniki[id].nick, _TRUNCATE);

				int id2 = tymczasowe_wyniki[0][d + 1];
				if (id2 == -1)
				{
					strncpy_s(wyniki[id].nick, 8, nick, _TRUNCATE);
				}
				else
				{
					strncpy_s(wyniki[id].nick, 8, wyniki[id2].nick, _TRUNCATE);
				}
				strncpy_s(wyniki[id].nick, 8, wyniki[id2].nick, _TRUNCATE);
				strncpy_s(wyniki[id2].nick, 8, nick_temp, _TRUNCATE);

				tymczasowe_wyniki[0][d] = tymczasowe_wyniki[0][d + 1];
				tymczasowe_wyniki[1][d] = tymczasowe_wyniki[1][d + 1];
				tymczasowe_wyniki[0][d + 1] = id;
				tymczasowe_wyniki[1][d + 1] = swap;
			}
		}
	}

	for (size_t i = 0; i < 5; i++)
	{
		wyniki[i].ID = tymczasowe_wyniki[i][0];
		wyniki[i].wynik = tymczasowe_wyniki[i][1];
	}

	for (size_t i = 0; i < 5; i++)
	{
		fprintf(tablica_wynikow, "%i.%s %i\n", wyniki[i].ID, wyniki[i].nick, wyniki[i].wynik);
	}

	fclose(tablica_wynikow);
}