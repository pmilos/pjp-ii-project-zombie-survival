#include "data.h"
#include "Nowa_Gra.h"
#include "Objekty.h"
#include "config.h"
#include "Tablica_wynikow.h"

extern int width;
extern int height;
extern opcje_gry dane_gry;
extern gracz wyniki[5];
extern punkty;

void menu(ALLEGRO_DISPLAY *display);
void opcje(ALLEGRO_DISPLAY *display);

void rysuj_menu(ALLEGRO_DISPLAY *display, int width, int height)
{
	ALLEGRO_BITMAP *tlo = NULL;
	ALLEGRO_BITMAP *p_nowa_gra = NULL;
	ALLEGRO_BITMAP *p_tablica = NULL;
	ALLEGRO_BITMAP *p_opcje = NULL;
	ALLEGRO_BITMAP *p_wyjscie = NULL;

	tlo = al_load_bitmap("Resources\\Tlo1.png");
	p_nowa_gra = al_load_bitmap("Resources\\Nowa_Gra.png");
	p_tablica = al_load_bitmap("Resources\\Tablica_wynikow.png");
	p_opcje = al_load_bitmap("Resources\\Opcje.png");
	p_wyjscie = al_load_bitmap("Resources\\Wyjscie.png");

	al_draw_bitmap(tlo, 0, 0, 0);
	al_draw_bitmap(p_nowa_gra, (width - 1050), (height - 650), 0);
	al_draw_bitmap(p_tablica, (width - 1050), (height - 550), 0);
	al_draw_bitmap(p_opcje, (width - 1050), (height - 450), 0);
	al_draw_bitmap(p_wyjscie, (width - 1050), (height - 350), 0);

	al_flip_display();

	al_destroy_bitmap(tlo);
	al_destroy_bitmap(p_nowa_gra);
	al_destroy_bitmap(p_tablica);
	al_destroy_bitmap(p_opcje);
	al_destroy_bitmap(p_wyjscie);

}
void rysuj_opcje(ALLEGRO_DISPLAY *display, int width, int height)
{
	ALLEGRO_BITMAP *tlo = NULL;
	ALLEGRO_FONT *font = NULL;
	ALLEGRO_BITMAP *bohater1 = NULL;
	ALLEGRO_BITMAP *bohater0 = NULL;

	al_init_font_addon();
	al_init_ttf_addon();

	tlo = al_load_bitmap("Resources\\Tlo2.png");
	font = al_load_font("Resources\\seguibl.ttf", 35, 0);
	bohater0 = al_load_bitmap("Resources\\bohater_1.1.png");
	bohater1 = al_load_bitmap("Resources\\bohater_2.1.png");

	al_draw_bitmap(tlo, 0, 0, 0);

	al_draw_textf(font, al_map_rgb(255, 255, 255), (width / 2) - 200, (height / 2) - 200, 0, "Wybor koloru bohatera");
	al_draw_textf(font, al_map_rgb(50, 205, 50), (width / 2) - 300, (height / 2), 0, "Zielony bohater");
	al_draw_textf(font, al_map_rgb(0, 0, 0), (width / 2) + 50, (height / 2), 0, "Czarny bohater");

	al_convert_mask_to_alpha(bohater1, al_map_rgb(255, 255, 255));
	al_convert_mask_to_alpha(bohater0, al_map_rgb(255, 255, 255));
	al_draw_bitmap(bohater1, (width / 2) - 175, (height / 2) + 50, 0);
	al_draw_bitmap(bohater0, (width / 2) + 150, (height / 2) + 50, 0);

	al_draw_bitmap(tlo, 0, 0, 0);
	al_flip_display();

	al_destroy_bitmap(tlo);
}
void obsluga_tablicy(ALLEGRO_DISPLAY *display, char *str)
{
	ALLEGRO_EVENT_QUEUE *queue = NULL;
	ALLEGRO_FONT *font = NULL;
	ALLEGRO_BITMAP *tlo = NULL;

	bool done = false;

	al_install_keyboard();
	al_init_font_addon();
	al_init_ttf_addon();


	queue = al_create_event_queue();
	tlo = al_load_bitmap("Resources\\Tlo2.png");
	font = al_load_font("Resources\\seguibl.ttf", 35, 0);

	al_register_event_source(queue, al_get_keyboard_event_source());

	size_t pos = strlen(str);
	al_draw_bitmap(tlo, 0, 0, 0);
	al_draw_text(font, al_map_rgb(50, 205, 50), (width / 2) - 500, (height - 150), 0, "Wpisz swoj nick ,a nastepnie potwierdz klawiszem ENTER");
	al_flip_display();

	while (!done)
	{
		ALLEGRO_EVENT ev;
		al_wait_for_event(queue, &ev);
		if (ev.type == ALLEGRO_EVENT_KEY_DOWN)
		{
			if (strlen(str) < 9)
			{
				switch (ev.keyboard.keycode)
				{
				case ALLEGRO_KEY_Q:
					str[pos] = 'Q';
					pos++;
					break;
				case ALLEGRO_KEY_W:
					str[pos] = 'W';
					pos++;
					break;
				case ALLEGRO_KEY_E:
					str[pos] = 'E';
					pos++;
					break;
				case ALLEGRO_KEY_R:
					str[pos] = 'R';
					pos++;
					break;
				case ALLEGRO_KEY_T:
					str[pos] = 'T';
					pos++;
					break;
				case ALLEGRO_KEY_Y:
					str[pos] = 'Y';
					pos++;
					break;
				case ALLEGRO_KEY_U:
					str[pos] = 'U';
					pos++;
					break;
				case ALLEGRO_KEY_I:
					str[pos] = 'I';
					pos++;
					break;
				case ALLEGRO_KEY_O:
					str[pos] = 'O';
					pos++;
					break;
				case ALLEGRO_KEY_P:
					str[pos] = 'P';
					pos++;
					break;
				case ALLEGRO_KEY_A:
					str[pos] = 'A';
					pos++;
					break;
				case ALLEGRO_KEY_S:
					str[pos] = 'S';
					pos++;
					break;
				case ALLEGRO_KEY_D:
					str[pos] = 'D';
					pos++;
					break;
				case ALLEGRO_KEY_F:
					str[pos] = 'F';
					pos++;
					break;
				case ALLEGRO_KEY_G:
					str[pos] = 'G';
					pos++;
					break;
				case ALLEGRO_KEY_H:
					str[pos] = 'H';
					pos++;
					break;
				case ALLEGRO_KEY_J:
					str[pos] = 'J';
					pos++;
					break;
				case ALLEGRO_KEY_K:
					str[pos] = 'K';
					pos++;
					break;
				case ALLEGRO_KEY_L:
					str[pos] = 'L';
					pos++;
					break;
				case ALLEGRO_KEY_Z:
					str[pos] = 'Z';
					pos++;
					break;
				case ALLEGRO_KEY_X:
					str[pos] = 'X';
					pos++;
					break;
				case ALLEGRO_KEY_C:
					str[pos] = 'C';
					pos++;
					break;
				case ALLEGRO_KEY_V:
					str[pos] = 'V';
					pos++;
					break;
				case ALLEGRO_KEY_B:
					str[pos] = 'B';
					pos++;
					break;
				case ALLEGRO_KEY_N:
					str[pos] = 'N';
					pos++;
					break;
				case ALLEGRO_KEY_M:
					str[pos] = 'M';
					pos++;
					break;
				case ALLEGRO_KEY_BACKSPACE:
					pos--;
					str[pos] = '\0';
					break;
				case ALLEGRO_KEY_ENTER:
					sortuj_wyniki(display, str, punkty);
					return;
				default:
					break;
				}
			}
			else
			{
				switch (ev.keyboard.keycode)
				{
				case ALLEGRO_KEY_BACKSPACE:
					pos--;
					str[pos] = '\0';
					break;
				case ALLEGRO_KEY_ENTER:
					sortuj_wyniki(display, str, punkty);
					return;
				default:
					break;
				}
			}

			al_draw_bitmap(tlo, 0, 0, 0);
			al_draw_text(font, al_map_rgb(50, 205, 50), (width / 2) - 500, (height - 150), 0, "Wpisz swoj nick ,a nastepnie potwierdz klawiszem ENTER");
			al_draw_textf(font, al_map_rgb(50, 205, 50), (width / 2) - 150, (height / 2), 0, "%s", str);
			al_flip_display();

		}
	}
	al_destroy_bitmap(tlo);
	al_destroy_event_queue(queue);
}


void opcje(ALLEGRO_DISPLAY *display)
{
	ALLEGRO_EVENT_QUEUE *queue = NULL;
	ALLEGRO_TIMER *timer = NULL;
	//ALLEGRO_SAMPLE *audio = NULL;


	bool done = false;

	int x = 0;
	int y = 0;

	al_get_mouse_cursor_position(&x, &y); //Pobierz pozycje kursora - obecna pozycja

	queue = al_create_event_queue();
	timer = al_create_timer(1.0 / FPS);
	//audio = al_load_sample("audio.ogg");

	al_register_event_source(queue, al_get_display_event_source(display));
	al_register_event_source(queue, al_get_timer_event_source(timer));
	al_register_event_source(queue, al_get_mouse_event_source());
	//al_play_sample(audio, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_LOOP, NULL);
	al_start_timer(timer);




	while (!done)
	{
		ALLEGRO_EVENT ev;
		al_wait_for_event(queue, &ev);

		if (ev.type == ALLEGRO_EVENT_MOUSE_AXES)
		{
			x = ev.mouse.x;
			y = ev.mouse.y;
		}

		else if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
		{
			if (ev.mouse.button & 1 && x > (width / 2) - 300 && x < (width / 2) - 100 && y >(height / 2) - 20 && y < (height / 2))
			{
				dane_gry.kolor_bohatera = 1;
				set_config(display, 1);
				rysuj_menu(display, width, height);
				menu(display);
				if (dane_gry.wyjdz_z_gry)
				{
					exit(0);
				}
			}
		}
	}
	al_destroy_timer(timer);
	al_destroy_event_queue(queue);
}

void _tablica_wynikow(ALLEGRO_DISPLAY *display)
{
	ALLEGRO_EVENT_QUEUE *queue = NULL;
	ALLEGRO_FONT *font = NULL;
	ALLEGRO_BITMAP *tlo = NULL;

	bool done = false;

	int x = 0;
	int y = 0;

	al_init_font_addon();
	al_init_ttf_addon();
	al_install_mouse();

	al_get_mouse_cursor_position(&x, &y); //Pobierz pozycje kursora - obecna pozycja

	tlo = al_load_bitmap("Resources\\Tlo2.png");
	queue = al_create_event_queue();
	font = al_load_font("Resources\\seguibl.ttf", 35, 0);

	al_register_event_source(queue, al_get_display_event_source(display));
	al_register_event_source(queue, al_get_mouse_event_source());

	al_draw_bitmap(tlo, 0, 0, 0);
	al_draw_text(font, al_map_rgb(50, 205, 50), 100, (height - 150), 0, "Powrot");
	al_draw_text(font, al_map_rgb(50, 205, 50), (width / 2) - 200, 100, 0, "Tablica Wynikow:");

	int pos = 150;
	for (size_t i = 0; i < 5; i++)
	{
		al_draw_textf(font, al_map_rgb(50, 205, 50), (width / 2) - 200, pos, 0, "%i.%s %i", wyniki[i].ID, wyniki[i].nick, wyniki[i].wynik);
		pos += 50;
	}

	al_flip_display();

	while (!done)
	{
		ALLEGRO_EVENT ev;
		al_wait_for_event(queue, &ev);

		if (ev.type == ALLEGRO_EVENT_MOUSE_AXES)
		{
			x = ev.mouse.x;
			y = ev.mouse.y;
		}

		else if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
		{
			if (ev.mouse.button & 1 && x > 100 && x < 200 && y >(height - 200) && y < (height - 150))
			{
				rysuj_menu(display, width, height);
				menu(display);
				if (dane_gry.wyjdz_z_gry)
				{
					exit(0);
				}
			}
		}
	}
	al_destroy_bitmap(tlo);
	al_destroy_event_queue(queue);
}

void menu(ALLEGRO_DISPLAY *display)
{
	if (dane_gry.wyjdz_z_gry)
	{
		return;
	}

	ALLEGRO_EVENT_QUEUE *queue = NULL;
	ALLEGRO_TIMER *timer = NULL;
	//ALLEGRO_SAMPLE *audio = NULL;


	bool done = false;

	int x = 0;
	int y = 0;

	char str[8] = { 0 };

	al_get_mouse_cursor_position(&x, &y); //Pobierz pozycje kursora - obecna pozycja



	queue = al_create_event_queue();
	timer = al_create_timer(1.0 / FPS);
	//audio = al_load_sample("audio.ogg");

	al_register_event_source(queue, al_get_display_event_source(display));
	al_register_event_source(queue, al_get_timer_event_source(timer));
	al_register_event_source(queue, al_get_mouse_event_source());
	//al_play_sample(audio, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_LOOP, NULL);
	al_start_timer(timer);




	while (!done)
	{
		ALLEGRO_EVENT ev;
		al_wait_for_event(queue, &ev);

		if (ev.type == ALLEGRO_EVENT_MOUSE_AXES)
		{
			x = ev.mouse.x;
			y = ev.mouse.y;
		}
		/*else if (ev.type == ALLEGRO_EVENT_TIMER)
		{

		}*/

		else if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
		{
			if (ev.mouse.button & 1 && x > (0.36)*(width) && x < (0.60)*(width) && y >(0.75)*(height) && y < (0.84)*(height))
			{
				done = true;
				dane_gry.wyjdz_z_gry = true;
				dane_gry.gra_dziala = false;
				dane_gry.nowa_gra = false;
			}
			else if (ev.mouse.button & 1 && x >(0.36)*(width) && x < (0.60)*(width) && y >(0.36)*(height) && y < (0.45)*(height))
			{
				al_destroy_timer(timer);
				al_destroy_event_queue(queue);
				dane_gry.nowa_gra = true;
				dane_gry.gra_dziala = true;
				Nowa_Gra(display, width, height);
				return;
			}
			else if (ev.mouse.button & 1 && x >(0.36)*(width) && x < (0.60)*(width) && y >(0.62)*(height) && y < (0.71)*(height))
			{
				al_destroy_timer(timer);
				al_destroy_event_queue(queue);
				rysuj_opcje(display, width, height);
				opcje(display);
			}
			else if (ev.mouse.button & 1 && x >(0.36)*(width) && x < (0.60)*(width) && y >(0.48)*(height) && y < (0.58)*(height))
			{
				al_destroy_timer(timer);
				al_destroy_event_queue(queue);
				_tablica_wynikow(display, str);

				return;
			}
		}

	}


	al_destroy_timer(timer);
	al_destroy_event_queue(queue);
}