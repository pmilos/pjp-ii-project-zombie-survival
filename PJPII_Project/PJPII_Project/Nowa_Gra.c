#include "data.h"
#include "Menu.h"
#include "Objekty.h"
#include <allegro5\allegro_primitives.h>
#include <math.h>
#include <stdio.h>

extern int width;
extern int height;
extern opcje_gry dane_gry;

int liczba_naboi = 25;
int liczba_zombie = 35;
int liczba_martwych_zombie = 0;
int liczba_naboi_zuzytych = 0;
int liczba_wystrzelonych = 0;
int punkty = 0;


//Rozgrywka
void zatrzymaj_gre(ALLEGRO_DISPLAY *display, ALLEGRO_FONT *Duzy_font)
{
	ALLEGRO_EVENT_QUEUE *queue = NULL;
	ALLEGRO_TIMER *timer = NULL;

	queue = al_create_event_queue();
	timer = al_create_timer(1.0 / FPS);

	al_register_event_source(queue, al_get_timer_event_source(timer));
	al_register_event_source(queue, al_get_mouse_event_source());
	al_start_timer(timer);

	bool done = false;
	while (!done)
	{
		ALLEGRO_EVENT ev;
		al_wait_for_event(queue, &ev);

		al_draw_textf(Duzy_font, al_map_rgb(255, 255, 255), (width / 2), (height / 2) - 50, 0, "Wroc do gry");
		al_draw_textf(Duzy_font, al_map_rgb(255, 255, 255), (width / 2), (height / 2) + 50, 0, "Wyjdz do menu");
		al_flip_display();

		if (ev.mouse.button & 1 && ev.mouse.x > (0.36)*(width) && ev.mouse.x < (0.60)*(width) && ev.mouse.y >(0.45)*(height) && ev.mouse.y < (0.55)*(height))
		{
			dane_gry.gra_dziala = true;
			if (dane_gry.gra_dziala)
			{
				return;
			}
		}
		else if (ev.mouse.button & 1 && ev.mouse.x >(0.36)*(width) && ev.mouse.x < (0.60)*(width) && ev.mouse.y >(0.6)*(height) && ev.mouse.y < (0.75)*(height))
		{
			if (dane_gry.wyjdz_z_gry)
			{
				return;
			}
			rysuj_menu(display, width, height);
			menu(display);
		}
	}
	al_destroy_timer(timer);
	al_destroy_event_queue(queue);
}
void koniec_gry(ALLEGRO_DISPLAY *display, postac *bohaterr)
{
	ALLEGRO_EVENT_QUEUE *queue = NULL;
	ALLEGRO_TIMER *timer = NULL;
	ALLEGRO_FONT *font = NULL;

	queue = al_create_event_queue();
	timer = al_create_timer(1.0 / FPS);
	font = al_load_font("Resources\\segoeui.ttf", 50, 0);

	al_register_event_source(queue, al_get_timer_event_source(timer));
	al_register_event_source(queue, al_get_mouse_event_source());
	al_start_timer(timer);

	char str[8] = { 0 };

	bool done = false;
	while (!done)
	{
		ALLEGRO_EVENT ev;
		al_wait_for_event(queue, &ev);

		al_draw_textf(font, al_map_rgb(255, 255, 255), (width / 2) - 100, (height / 2) - 100, 0, "KONIEC GRY");
		al_draw_textf(font, al_map_rgb(255, 255, 255), (width / 2) - 100, (height / 2) - 25, 0, "Punkty: %i", bohaterr[0].punkty);

		al_flip_display();

		al_rest(7);
		obsluga_tablicy(display, str);
		_tablica_wynikow(display);
		if (dane_gry.wyjdz_z_gry)
		{
			return;
		}
	}
	al_destroy_timer(timer);
	al_destroy_event_queue(queue);
}

//Bohater
void InitBohaterr(postac *bohaterr)
{
	memset((*bohaterr).key, 0, 5);
	(*bohaterr).ID = bohater;
	(*bohaterr).x = (0.5)*(width);
	(*bohaterr).y = (0.5)*(height);
	(*bohaterr).predkosc = 3;
	(*bohaterr).punkty_zycia = 10;
	(*bohaterr).boundx = 14;
	(*bohaterr).boundy = 17;
	(*bohaterr).punkty = 0;
	(*bohaterr).kat = 0;
}
void DrawB(ALLEGRO_BITMAP *bohater, postac *bohaterr)
{


	//al_draw_rectangle(0, 0, 15, 15, al_map_rgb(0, 0, 255), 3.0);
	al_convert_mask_to_alpha(bohater, al_map_rgb(255, 255, 255));
	al_draw_rotated_bitmap(bohater, +14, +17, bohaterr->x, bohaterr->y, bohaterr->kat, 0);
	//al_draw_bitmap(bohater, (bohaterr->x) - 14, (bohaterr->y) - 17, 0);


}
void Move(postac *bohaterr)
{
	(*bohaterr).y -= (*bohaterr).predkosc * bohaterr->key[up];
	(*bohaterr).y += (*bohaterr).predkosc * bohaterr->key[down];
	(*bohaterr).x -= (*bohaterr).predkosc * bohaterr->key[left];
	(*bohaterr).x += (*bohaterr).predkosc * bohaterr->key[right];


	if (bohaterr->y < 0)
		(*bohaterr).y = 0;
	if (bohaterr->y > height)
		(*bohaterr).y = height;
	if (bohaterr->x < 0)
		(*bohaterr).x = 0;
	if (bohaterr->x > width)
		(*bohaterr).x = width;
}
void RotateB(postac *bohaterr, int mx, int my)
{
	(*bohaterr).kat = atan2f(my - (*bohaterr).y, mx - (*bohaterr).x);
}


//Naboje
void InitNaboj(naboje nabojee[], int size)
{

	for (int i = 0; i < size; i++)
	{
		nabojee[i].ID = naboj;
		nabojee[i].predkosc = 5;
		nabojee[i].aktywny = false;
	}
}
void DrawNaboj(naboje nabojee[], int size)
{

	for (size_t i = 0; i != size; i++)
	{
		if (nabojee[i].aktywny)
		{
			al_draw_filled_circle(nabojee[i].x, nabojee[i].y, 3, al_map_rgb(255, 255, 0));
		}

	}
}
void FireNaboj(naboje nabojee[], int size, postac *bohaterr)
{

	if ((liczba_naboi_zuzytych != liczba_naboi) && (liczba_wystrzelonych != liczba_naboi) && (liczba_wystrzelonych + liczba_naboi_zuzytych != liczba_naboi))
	{
		for (size_t i = 0; i != size; i++)
		{
			if (!(nabojee[i].aktywny))
			{
				liczba_wystrzelonych++;
				nabojee[i].x = (*bohaterr).x;
				nabojee[i].kat = (*bohaterr).kat;
				nabojee[i].y = (*bohaterr).y;
				nabojee[i].aktywny = true;
				break;
			}

		}
	}

}
void UpdateNaboj(naboje nabojee[], int size)
{

	for (size_t i = 0; i != size; i++)
	{
		if (nabojee[i].aktywny)
		{

			float kat = (nabojee[i].kat) * (180 / ALLEGRO_PI);
			if (kat < 0 && kat > -90) //I
			{
				nabojee[i].x += nabojee[i].predkosc;
				nabojee[i].y += nabojee[i].predkosc * tan(nabojee[i].kat); // X-, y+ = III X+ y+ = I X- , Y- = II X+ Y- = IV
				//printf("Kat ujemny lub 0!\n");
			}
			else if (kat > 0 && kat < 90) //III
			{
				nabojee[i].x += nabojee[i].predkosc;
				nabojee[i].y -= -(nabojee[i].predkosc * tan(nabojee[i].kat));
			}

			else if (kat > 90 && kat < 180) //IV
			{
				nabojee[i].x -= nabojee[i].predkosc;
				nabojee[i].y -= nabojee[i].predkosc * tan(nabojee[i].kat);
			}

			else if (kat > -180 && kat < -90) //II
			{
				nabojee[i].x -= nabojee[i].predkosc;
				nabojee[i].y += -(nabojee[i].predkosc * tan(nabojee[i].kat));
			}

			else if (kat == 0)
			{
				nabojee[i].x += nabojee[i].predkosc;
			}

			else if (kat == 90)
			{
				nabojee[i].y += nabojee[i].predkosc;
			}

			else if (kat == 180)
			{
				nabojee[i].x -= nabojee[i].predkosc;
			}

			else if (kat == -90)
			{
				nabojee[i].y -= nabojee[i].predkosc;
			}

			if ((nabojee[i].x > width || nabojee[i].x < 0) || (nabojee[i].y > height || nabojee[i].y < 0))
			{
				nabojee[i].aktywny = false;
				liczba_naboi_zuzytych++;
				liczba_wystrzelonych--;
				if (liczba_naboi_zuzytych > 25) liczba_naboi_zuzytych = 25;
			}
		}

	}
}
void CollideNaboj(naboje nabojee[], int nSize, zombi zombiee[], int zSize, postac bohater[])
{

	for (size_t i = 0; i != nSize; i++)
	{
		if (nabojee[i].aktywny)
		{
			for (size_t j = 0; j != zSize; j++)
			{
				if (zombiee[j].aktywny)
				{
					if (nabojee[i].x > (zombiee[j].x - zombiee[j].boundx) &&
						nabojee[i].x < (zombiee[j].x + zombiee[j].boundx) &&
						nabojee[i].y >(zombiee[j].y - zombiee[j].boundy) &&
						nabojee[i].y < (zombiee[j].y + zombiee[j].boundy))
					{
						zombiee[j].aktywny = false;
						zombiee[j].nie_zywy = true;
						liczba_wystrzelonych--;
						liczba_martwych_zombie++;
						nabojee[i].aktywny = false;
						bohater[0].punkty++;
						punkty++;
					}
				}

			}
		}

	}
}

//Zombie
void InitZombie(zombi zombiee[], int size)
{
	srand((unsigned)time(NULL));

	for (size_t i = 0; i != size; i++)
	{
		zombiee[i].ID = zombie;
		zombiee[i].predkosc = 2;
		zombiee[i].boundx = 20;
		zombiee[i].boundy = 20;
		zombiee[i].aktywny = false;
		zombiee[i].nie_zywy = false;
		zombiee[i].kat = 0;

	}

}
void DrawZombie(ALLEGRO_BITMAP *zombie, zombi zombiee[], int size, ALLEGRO_BITMAP *krew)
{

	for (size_t i = 0; i != size; i++)
	{
		if (zombiee[i].aktywny)
		{
			al_convert_mask_to_alpha(zombie, al_map_rgb(255, 255, 255));
			al_draw_rotated_bitmap(zombie, 18, 18, (zombiee[i].x), (zombiee[i].y), (zombiee[i].kat), 0);
		}

		else if (zombiee[i].nie_zywy)
		{
			al_draw_bitmap(krew, zombiee[i].x, zombiee[i].y, 0);
		}
	}

}
void StartZombie(zombi zombiee[], int size)
{
	int cords[][2] =
	{
		{ -50, 150 },
		{ -50, 350 },
		{ -50, 550 },
		{ -50, 700 },
		{ width + 50, 150 },
		{ width + 50, 350 },
		{ width + 50, 550 },
		{ width + 50, 700 },
		{ 100, -50 },
		{ 200, -50 },
		{ 300, -50 },
		{ 400, -50 },
		{ 500, -50 },
		{ 600, -50 },
		{ 700, -50 },
		{ 800, -50 },
		{ 900, -50 },
		{ 1000, -50 },
		{ 1100, -50 },
		{ 1200, -50 },
		{ 1300, -50 },
		{ 100, height + 50 },
		{ 200, height + 50 },
		{ 300, height + 50 },
		{ 400, height + 50 },
		{ 500, height + 50 },
		{ 600, height + 50 },
		{ 700, height + 50 },
		{ 800, height + 50 },
		{ 900, height + 50 },
		{ 1000, height + 50 },
		{ 1100, height + 50 },
		{ 1200, height + 50 },
		{ 1300, height + 50 },
	};


	for (size_t i = 0; i != size; i++)
	{
		if (!zombiee[i].aktywny && !zombiee[i].nie_zywy)
		{
			if (rand() % 400 == 0)
			{
				zombiee[i].aktywny = true;
				int randomCord = 0;
				randomCord = rand() % 34;
				zombiee[i].x = cords[randomCord][0];
				zombiee[i].y = cords[randomCord][1];
				break;
			}
		}

	}

}
void UpdateZombie(zombi zombiee[], int size, postac *bohaterr)
{
	float deltaX, deltaY = 0.0;
	int kat = 0;
	for (size_t i = 0; i != size; i++)
	{
		if (zombiee[i].aktywny)
		{

			deltaX = (float)(zombiee[i].x - (float)(*bohaterr).x);
			deltaY = (float)(zombiee[i].y - (float)(*bohaterr).y);
			kat = atan2(deltaY, deltaX);

			zombiee[i].x -= (zombiee[i].predkosc * cos(kat));
			zombiee[i].y -= (zombiee[i].predkosc * sin(kat));
		}

	}
}
void CollideZombie(zombi zombiee[], int zSize, postac bohaterr[])
{

	for (size_t i = 0; i != zSize; i++)
	{
		if (zombiee[i].aktywny)
		{
			if (zombiee[i].x - zombiee[i].boundx < bohaterr[0].x + bohaterr[0].boundx &&
				zombiee[i].x + zombiee[i].boundx > bohaterr[0].x + bohaterr[0].boundx &&
				zombiee[i].y - zombiee[i].boundx < bohaterr[0].y + bohaterr[0].boundy &&
				zombiee[i].y + zombiee[i].boundx > bohaterr[0].y + bohaterr[0].boundy
				)
			{
				bohaterr[0].punkty_zycia--;
				zombiee[i].aktywny = false;
				zombiee[i].nie_zywy = true;
				liczba_martwych_zombie++;




				if (bohaterr[0].punkty_zycia <= 0)
				{
					dane_gry.koniec_gry = true;
				}
			}
		}

	}
}
void RotateZ(zombi zombiee[], int size, postac *bohaterr)
{
	for (size_t i = 0; i != size; i++)
	{
		zombiee[i].kat = atan2((*bohaterr).y - zombiee[i].y, (*bohaterr).x - zombiee[i].x);
	}
}

void Nowa_Gra(ALLEGRO_DISPLAY *display, int width, int height)
{
	if (dane_gry.wyjdz_z_gry)
	{
		return;
	}

	if (dane_gry.nowa_gra)
	{
		liczba_martwych_zombie = 0;
		liczba_naboi_zuzytych = 0;
		liczba_wystrzelonych = 0;
	}

	ALLEGRO_EVENT_QUEUE *queue = NULL;
	ALLEGRO_TIMER *timer = NULL;

	//ALLEGRO_SAMPLE *audio = NULL;

	ALLEGRO_BITMAP *mapa = NULL;
	ALLEGRO_BITMAP *bohater = NULL;
	ALLEGRO_BITMAP *Zombie = NULL;
	ALLEGRO_BITMAP *krew = NULL;
	ALLEGRO_FONT *font = NULL;
	ALLEGRO_FONT *Duzy_font = NULL;


	mapa = al_load_bitmap("Resources\\Mapa_1.png");
	if (dane_gry.kolor_bohatera == 0)
	{
		bohater = al_load_bitmap("Resources\\bohater_1.1.png");
	}
	else if (dane_gry.kolor_bohatera == 1)
	{
		bohater = al_load_bitmap("Resources\\bohater_2.1.png");
	}
	else
	{
		bohater = al_load_bitmap("Resources\\bohater_1.1.png");
	}

	Zombie = al_load_bitmap("Resources\\zombie.png");
	krew = al_load_bitmap("Resources\\krew.png");

	naboje naboj[25] = { 0 };
	postac bohaterr = { 0 };
	zombi zombie[35] = { 0 };

	al_init_primitives_addon();
	al_init_font_addon();
	al_init_ttf_addon();


	InitNaboj(naboj, liczba_naboi);
	InitBohaterr(&bohaterr);
	InitZombie(zombie, liczba_zombie);

	bool done = false;

	bool render = true;

	int mx = 0;
	int my = 0;



	srand((unsigned)time(NULL));
	queue = al_create_event_queue();
	timer = al_create_timer(1.0 / FPS);
	font = al_load_font("Resources\\segoeui.ttf", 24, 0);
	Duzy_font = al_load_font("Resources\\segoeui.ttf", 35, 0);
	//audio = al_load_sample("audio.ogg");

	al_register_event_source(queue, al_get_keyboard_event_source());
	al_register_event_source(queue, al_get_display_event_source(display));
	al_register_event_source(queue, al_get_timer_event_source(timer));
	al_register_event_source(queue, al_get_mouse_event_source());
	//al_play_sample(audio, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_LOOP, NULL);
	al_start_timer(timer);

	while (!done)
	{
		ALLEGRO_EVENT ev;
		al_wait_for_event(queue, &ev);

		if (ev.type == ALLEGRO_EVENT_KEY_DOWN)
		{
			switch (ev.keyboard.keycode)
			{
			case ALLEGRO_KEY_W:
				bohaterr.key[up] = true;
				break;
			case ALLEGRO_KEY_S:
				bohaterr.key[down] = true;
				break;
			case ALLEGRO_KEY_A:
				bohaterr.key[left] = true;
				break;
			case ALLEGRO_KEY_D:
				bohaterr.key[right] = true;
				break;
			case ALLEGRO_KEY_SPACE:
				bohaterr.key[space] = true;
				break;
			default:
				break;
			}
		}
		else if (ev.type == ALLEGRO_EVENT_KEY_UP)
		{
			switch (ev.keyboard.keycode)
			{
			case ALLEGRO_KEY_W:
				bohaterr.key[up] = false;
				break;
			case ALLEGRO_KEY_S:
				bohaterr.key[down] = false;
				break;
			case ALLEGRO_KEY_A:
				bohaterr.key[left] = false;
				break;
			case ALLEGRO_KEY_D:
				bohaterr.key[right] = false;
				break;
			case ALLEGRO_KEY_ESCAPE:
				dane_gry.gra_dziala = false;
				if (dane_gry.gra_dziala)
				{
					al_start_timer(timer);
				}
				else
				{
					al_stop_timer(timer);
				}
				zatrzymaj_gre(display, Duzy_font);
				if (dane_gry.gra_dziala)
				{
					al_start_timer(timer);
				}
				if (dane_gry.wyjdz_z_gry)
				{
					return;
				}


				break;
			default:
				break;
			}
		}
		else if (ev.type == ALLEGRO_EVENT_MOUSE_AXES)
		{
			mx = ev.mouse.x;
			my = ev.mouse.y;
		}
		else if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
		{
			if (ev.mouse.button & 1)
			{
				FireNaboj(naboj, liczba_naboi, &bohaterr);
			}
		}
		else if (ev.type == ALLEGRO_EVENT_TIMER)
		{
			if (liczba_martwych_zombie == liczba_zombie)
			{
				InitZombie(zombie, liczba_zombie);
				liczba_martwych_zombie = 0;
			}

			Move(&bohaterr);

			RotateB(&bohaterr, mx, my);
			RotateZ(zombie, liczba_zombie, &bohaterr);

			UpdateNaboj(naboj, liczba_naboi);

			StartZombie(zombie, liczba_zombie);
			UpdateZombie(zombie, liczba_zombie, &bohaterr);
			CollideNaboj(naboj, liczba_naboi, zombie, liczba_zombie, &bohaterr);
			CollideZombie(zombie, liczba_zombie, &bohaterr);
			if (dane_gry.koniec_gry)
			{
				koniec_gry(display, &bohaterr);
			}
			render = true;
		}

		if (render && al_is_event_queue_empty(queue))
		{

			al_draw_bitmap(mapa, 0, 0, 0);

			DrawB(bohater, &bohaterr);

			al_draw_textf(font, al_map_rgb(255, 255, 255), 0, 0, 0, "Punkty: %i", bohaterr.punkty);
			al_draw_textf(font, al_map_rgb(255, 255, 255), 0, 20, 0, "Punkty Zycia: %i", bohaterr.punkty_zycia);
			al_draw_textf(font, al_map_rgb(255, 255, 255), 0, 40, 0, "Liczba naboi: %i", liczba_naboi - liczba_naboi_zuzytych);

			DrawNaboj(naboj, liczba_naboi);
			DrawZombie(Zombie, zombie, liczba_zombie, krew);


			al_flip_display();
			render = false;
		}
	}


	al_destroy_bitmap(bohater);
	al_destroy_timer(timer);
	al_destroy_event_queue(queue);
}