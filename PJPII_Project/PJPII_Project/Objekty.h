#ifndef objekty_h
#define objekty_h

enum IDS{ bohater, naboj, zombie };

enum KEYS { up, down, left, right, space };

typedef struct bohater
{
	bool key[5];
	int ID;
	float x;
	float y;
	int predkosc;
	int punkty_zycia;
	int boundx;
	int boundy;
	int punkty;
	float kat;
} postac;


typedef struct nabojee
{
	int ID;
	float x;
	float y;
	bool aktywny;
	int predkosc;
	float kat;
}naboje;

typedef struct zombiee
{
	int ID;
	float x;
	float y;
	int predkosc;
	bool aktywny;
	bool nie_zywy;
	int boundx;
	int boundy;
	float kat;

}zombi;

typedef struct game_options
{
	bool wyjdz_z_gry;
	bool nowa_gra;
	bool gra_dziala;
	bool koniec_gry;
	int kolor_bohatera;
} opcje_gry;

typedef struct tablica_wynikow
{
	int ID;
	char nick[8];
	int wynik;
} gracz;


#endif