#include "data.h"
#include "Menu.h"
#include "Objekty.h"
#include "config.h"
#include "Tablica_wynikow.h"


int width = 1366;
int height = 768;

opcje_gry dane_gry = { 0 };
extern gracz wyniki[5];


int main()
{
	ALLEGRO_DISPLAY *display = NULL;

	if (!al_init())
	{
		al_show_native_message_box(display, "Error", 0, "Failed to initialise Allegro!", 0, 0);
		return -1;
	}

	al_init_image_addon();
	al_set_new_display_flags(ALLEGRO_NOFRAME | ALLEGRO_FULLSCREEN_WINDOW);

	display = al_create_display(width, height);

	if (!display)
	{
		al_show_native_message_box(display, "Error", 0, "Failed to create display!", 0, 0);
		return -1;
	}

	al_install_mouse();
	al_install_keyboard();
	//al_install_audio();
	//al_init_acodec_addon();
	al_init_native_dialog_addon();
	//al_reserve_samples(1);




	al_set_window_title(display, "Zombie Survival");

	al_clear_to_color(al_map_rgb(0, 0, 0));

	al_flip_display();

	config(display);
	tablica_wynikow(display);

	rysuj_menu(display, width, height);


	menu(display);





	al_destroy_display(display);
	return 0;
}