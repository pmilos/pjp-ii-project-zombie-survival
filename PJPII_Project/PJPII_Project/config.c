#include <stdlib.h>
#include <stdio.h>
#include "data.h"
#include "Objekty.h"

extern opcje_gry dane_gry;

void config(ALLEGRO_DISPLAY *display)
{
	FILE *config = NULL;

	errno_t err = fopen_s(&config, "config.ini", "r");

	if (err)
	{
		FILE *config_create = NULL;
		err = fopen_s(&config_create, "config.ini", "w");

		if (err)
		{
			al_show_native_message_box(display, "Error", "Error", "Nie udalo sie stworzyc pliku config.ini", 0, ALLEGRO_MESSAGEBOX_ERROR);
			exit(EXIT_FAILURE);
		}

		fprintf(config_create, "kolor_bohatera = 0\n");
		dane_gry.kolor_bohatera = 0;

		fclose(config_create);
		return;
	}
	char config_str[32] = { 0 };
	fgets(config_str, sizeof(config_str), config);

	sscanf_s(config_str, "%*s %*s %i", &dane_gry.kolor_bohatera);

	fclose(config);

	return;
}
void set_config(ALLEGRO_DISPLAY *display, int kolor)
{
	FILE *config = NULL;

	errno_t err = fopen_s(&config, "config.ini", "w");

	if (err)
	{
		al_show_native_message_box(display, "Error", "Error", "Nie udalo sie otworzyc pliku config.ini", 0, ALLEGRO_MESSAGEBOX_ERROR);
		exit(EXIT_FAILURE);
	}

	fprintf(config, "kolor_bohatera = %i\n", kolor);

	fclose(config);

	return;
}